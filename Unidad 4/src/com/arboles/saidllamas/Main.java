package com.arboles.saidllamas;

import java.util.Scanner;

public class Main {
	
	private static Arbol operacionesArbol = new Arbol();
	
	public static void main(String[] args){
		System.out.println("¡Bienvenido!");
		menu();
	}
	
	static void menu(){
		int opc = -1;
		do{
			System.out.println("MENÚ");
			System.out.println("Elija operacion: ");
			System.out.println("1) Crear ");
			System.out.println("2) Insertar ");
			System.out.println("3) Buscar ");
			System.out.println("4) Recorrido Pre-Orden ");
			System.out.println("5) Recorrido In-Orden ");
			System.out.println("6) Recorrido Pos-Orden ");
			System.out.println("7) Eliminar ");
			System.out.println("[8] Terminar. ");
			opc = recibirDato(); //mando llamar a la entrada
			if(opc > 0 && opc < 8) controlador(opc);
			else if(opc == -1) System.out.print(""); //opcion default-cuando entra una letra
			else if(opc == 8){
				System.out.println("Fin del programa.");
				break;
			}
			else{
				System.err.println("Ingrese una opcion desde el 1 - 8");
			}
		}while(opc != 8);
	}
	
	//llama a los metodos de la clase Arbol siempre y cuando haya datos que manipular
	static void controlador(int ID){
		int tempDato = -1;
		if(ID != 1 && operacionesArbol.isEmpty() == false){
			if(ID == 2 || ID == 3 || ID == 7){	tempDato = recibirDato(); } //Se requiere una entrada
			switch(ID){
			case 2:
				operacionesArbol.insertar(tempDato);
				break;
			case 3:
				operacionesArbol.buscar(tempDato);
				break;
			case 4:
				operacionesArbol.ordenamiento(1); //Pre
				break;
			case 5:
				operacionesArbol.ordenamiento(2); //In
				break;
			case 6:
				operacionesArbol.ordenamiento(3); //Pos
				break;
			case 7:
				operacionesArbol.eliminar(tempDato);
				break;
			}
		}else if(ID == 1){ //Si esta vacio entonces abrir paso a crearlo
			tempDato = recibirDato();
			operacionesArbol.crear(tempDato);
		}else if(operacionesArbol.isEmpty()){
			System.err.println("Ocurrio un error. El arbol no esta creado.");
		}
	}
	
	//validacion solo numeros de 1-100, retorna el valor recibido
	static int recibirDato(){
		int dato = -1;
		do{
			try {
				System.out.println("|..");
				Scanner entrada = new Scanner(System.in);
				dato = entrada.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error en la entrada");
			}
		}while(dato < 1 | dato > 100);
		return dato;
	}

}//end-Clas
