package com.arboles.saidllamas;

public class Arbol {
	
	private Nodo raiz = new Nodo(), p = new Nodo(), otro = new Nodo(),q = new Nodo(), r = new Nodo();
	
	public Arbol(){
		raiz = null;
		p = null;
		otro = null;
		q = null;
		r = null;
	}
	
	//Operaciones basicas 
	//Para llamar desde cualquier clase
	public void crear(int dato){
		try{
			if(p == null){
				Nodo nuevo = new Nodo();
				nuevo.setEnlaceD(null);
				nuevo.setEnlaceI(null);
				nuevo.setInformacion(dato);
				raiz = nuevo;
				p = raiz; //si lo agrego si funciona el algoritmo
				System.out.println("Arbol creado correctamente.");
			}else System.out.println("El arbol ya estaba creado.");
		}catch (Exception e) {
			System.err.println("Ocurrio un error tratando de crear el arbol.");
		}
	}//end-crear
	
	public void insertar(int dato){
		insertar(raiz, dato);
	}//end-insertar
	
	public void buscar(int dato){
		buscar(raiz, dato);
	}
	
	public void eliminar(int dato){
		eliminar(raiz, dato);
	}
	
	//Realizan las operaciones correspondientes al algoritmo
	private void insertar(Nodo p, int dato){
		try {
			if(p != null){
				if(dato < p.getInformacion()){
					if(p.getEnlaceI() == null){
						Nodo nuevo = new Nodo();
						nuevo.setEnlaceI(null);
						nuevo.setEnlaceD(null);
						nuevo.setInformacion(dato);
						p.setEnlaceI(nuevo);
					}else{
						insertar(p.getEnlaceI(), dato);
					}
				}else{
					if(dato > p.getInformacion()){
						if(p.getEnlaceD() == null){
							Nodo nuevo = new Nodo();
							nuevo.setEnlaceI(null);
							nuevo.setEnlaceD(null);
							nuevo.setInformacion(dato);
							p.setEnlaceD(nuevo);
						}else{
							insertar(p.getEnlaceD(), dato);
						}
					}
				}
			}else System.out.println("El abol no esta creado");
		} catch (Exception e) {
			System.err.println("Ocurrio un error tratando de insertar un nodo");
		}
	}
	
	private void buscar(Nodo p,int dato){
		try {
			if(p == null){ System.out.println("La clave no se encuentra en el arbol"); }
			else{
				if (dato == p.getInformacion()) System.out.println("Clave encontrada.");
				else if(dato < p.getInformacion()){
					q = p;
					buscar(p.getEnlaceI(), dato); //Recursividad
				}else{
					if(dato > p.getInformacion()){	q = p;	buscar(p.getEnlaceD(), dato);	} //Recursividad
				}
			}
		}catch(Exception e){
			System.err.println("Ocurrio un error durante la busqueda.");
		}
	}//end-buscar
	
	private void eliminar(Nodo p, int dato){
		try {
			if(p == null){ System.out.println("La clave no se encuentra en el arbol"); }
			else{
				if(dato < p.getInformacion()){
					q = p;
					eliminar(p.getEnlaceI(), dato); //Recursividad
				}else{
					if(dato > p.getInformacion()){	q = p;	eliminar(p.getEnlaceD(), dato);	} //Recursividad
					else{
						otro = p;
						if(otro.getEnlaceI() == null && otro.getEnlaceD() == null){
							if(otro == raiz) raiz = null;
							else{
								if(q.getEnlaceI() == p) q.setEnlaceI(p.getEnlaceI());
								else{
									if(q.getEnlaceD() == p) q.setEnlaceD(p.getEnlaceD());
								}
							}
						}//if-bisnieto
						else{
							if(otro.getEnlaceD() == null){
								if(otro == raiz){
									raiz = otro.getEnlaceI();
								}else{
									p = otro.getEnlaceI();
									otro.setInformacion(p.getInformacion());
									otro.setEnlaceI(p.getEnlaceI());
									otro.setEnlaceD(p.getEnlaceD());
									otro = p;
								}
							}else{
								if(otro.getEnlaceI() == null){
									if(otro == raiz) raiz = otro.getEnlaceD();
									else{
									p = otro.getEnlaceD();
									otro.setInformacion(p.getInformacion());
									otro.setEnlaceD(p.getEnlaceD());
									otro.setEnlaceI(p.getEnlaceI());
									otro = p;
									}
								}else{
									q = otro.getEnlaceI();
									r = q;
									while(q.getEnlaceD() != null){
										r = q;
										q  = q.getEnlaceD();
									}//end-while
									otro.setInformacion(q.getInformacion());
									otro = q; 
									r.setEnlaceD(q.getEnlaceI());
									if(p.getEnlaceI() == otro) p.setEnlaceI(otro.getEnlaceI());
									else if (p.getEnlaceD() == otro) p.setEnlaceD(otro.getEnlaceD());
								}
							}
						}//if-else-bisnieto
						this.p = null; //si no lo agrego no funciona eliminar e inmediatamente crear
						otro = null; p = null; q = null; r = null; //Librar memoria
						System.out.println("Eliminado correctamente.");
					}//else-nieto
				}//else-hijo
			}//else-padre
		} catch (Exception e) {
			System.err.println("Ocurrio un error durante la eliminacion");
		}
	}//end-eliminar
	//end-operaciones basicas
	
	//Ordenamiento
	public void ordenamiento(int ID){
		try {
			switch(ID){
				case 1: preOrden(raiz);
			break;
				case 2: inOrden(raiz);
			break;
				case 3: posOrden(raiz);
			break;
			}
		} catch (Exception e) {
			System.out.println("Hubo un error seleccionando el ordenamiento.");
		}
	}
	
	private void preOrden(Nodo p){
		try {
			if(p != null){
				System.out.println("["+p.getInformacion()+"]");
				preOrden(p.getEnlaceI());
				preOrden(p.getEnlaceD());
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}

	private void inOrden(Nodo p){
		try {
			if(p != null){
				inOrden(p.getEnlaceI());
				System.out.println("["+p.getInformacion()+"]");
				inOrden(p.getEnlaceD());
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}

	private void posOrden(Nodo p){
		try {
			if(p != null){
				posOrden(p.getEnlaceI());
				posOrden(p.getEnlaceD());
				System.out.println("["+p.getInformacion()+"]");
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}
	//end-ordenamientos

	//operaciones extra
	public boolean isEmpty(){
		if(raiz == null) return true;
		else return false;
	}
	
	//operaciones de examen
}//class