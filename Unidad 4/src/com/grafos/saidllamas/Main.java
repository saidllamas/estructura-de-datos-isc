package com.grafos.saidllamas;

import java.util.Scanner;

public class Main {
	
	private static int[][] grafo;

	public static void main(String[] args) {
		int tamaño = -1;
		System.out.println("¡Bienvenido/a!");
		do{
			System.out.println("Ingrese tamaño de la matriz. 1:8");
			try {
				Scanner entrada = new Scanner(System.in);
				tamaño = entrada.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error con la entrada.");
			}
		}while(tamaño <= 0 || tamaño > 8);
		grafo = new int[tamaño][tamaño];
		llenarMatriz(tamaño);
		generarMatriz(tamaño);
		armarParejas(tamaño);
	}//main
	
	
	private static void llenarMatriz(int tamaño){
		try {
			System.out.print("{");
			for(int i = 0; i < tamaño; i++){
				for(int x = 0; x < tamaño; x++){
					int estado = -1;
					do{
						System.out.print("("+generarLetra(i)+","+generarLetra(x)+") ");
						try{
							Scanner entrada = new Scanner(System.in);
							estado  = entrada.nextInt();
						}catch (Exception e) {
						}
					}while(estado < 0 || estado > 1);
					grafo[i][x] = estado;
				}
			}
			System.out.print("}");
		} catch (Exception e) {
			System.out.println("Ocurrio un error en el recorrido.");
		}
	}//end-llenarMatriz
	
	private static void generarMatriz(int tamaño){
		System.out.println(); System.out.println();
		System.out.print("    ");
		for(int i = 0; i < tamaño; i++){System.out.print(generarLetra(i)+"    ");}
		System.out.println();
		for(int i = 0; i < tamaño; i++){
			System.out.print(generarLetra(i)+"   ");
			for(int x = 0; x < tamaño; x++){
				System.out.print(grafo[i][x]+"    ");
			}
			System.out.println();
		}
	}//end-generarMatriz

	private static void armarParejas(int tamaño){
		System.out.println();
		System.out.print("{");
		try {
			for(int i = 0; i < tamaño; i++){
				for(int x = 0; x < tamaño; x++){
					if(grafo[i][x]==1){
						System.out.print("("+generarLetra(i)+","+generarLetra(x)+") ");
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Ocurrio un error en el recorrido del arreglo.");
		}
		System.out.print("}");
	}
	
	private static char generarLetra(int ID){
		switch(ID){
		case 0:	
			return 'A';
		case 1:
			return 'B';
		case 2:	
			return 'C';
		case 3:
			return 'D';
		case 4:	
			return 'E';
		case 5:
			return 'F';
		case 6:	
			return 'G';
		case 7:
			return 'H';
		default: return 'z';
		}
	}

}//class