package com.arbolesmejorados.saidllamas;

public class Arbol {
	
	private Nodo raiz = new Nodo(), p = new Nodo(), otro = new Nodo(),q = new Nodo(), r = new Nodo();
	private static int nivelI, nivelD;
	private static int nodosItemp = 0, nodosDtemp = 0;
	
	public Arbol(){
		raiz = null;
		p = null;
		otro = null;
		q = null;
		r = null;
		nivelI = 0; //nivel base
		nivelD = 0; //nivel base
	}
	
	//Operaciones basicas 
	//Para llamar desde cualquier clase
	public void crear(int dato){
		try{
			if(p == null){
				Nodo nuevo = new Nodo();
				nuevo.setEnlaceD(null);
				nuevo.setEnlaceI(null);
				nuevo.setInformacion(dato);
				raiz = nuevo;
				p = raiz; //si lo agrego si funciona el algoritmo
				System.out.println("Arbol creado correctamente.");
				nivelI = 1; nivelD = 1; // cuando se crea un arbol la base es 1 para su nivel
			}else System.out.println("El arbol ya estaba creado.");
		}catch (Exception e) {
			System.err.println("Ocurrio un error tratando de crear el arbol.");
		}
	}//end-crear
	
	public void insertar(int dato){
		insertar(raiz, dato);
	}//end-insertar
	
	public void buscar(int dato){
		buscar(raiz, dato);
	}
	
	public void eliminar(int dato){
		eliminar(raiz, dato);
	}
	
	//Realizan las operaciones correspondientes al algoritmo
	private void insertar(Nodo p, int dato){
		try {
			if(p != null){
				if(dato < p.getInformacion()){
					if(p.getEnlaceI() == null){
						Nodo nuevo = new Nodo();
						nuevo.setEnlaceI(null);
						nuevo.setEnlaceD(null);
						nuevo.setInformacion(dato);
						p.setEnlaceI(nuevo);
					}else{
						insertar(p.getEnlaceI(), dato);
					}
				}else{
					if(dato > p.getInformacion()){
						if(p.getEnlaceD() == null){
							Nodo nuevo = new Nodo();
							nuevo.setEnlaceI(null);
							nuevo.setEnlaceD(null);
							nuevo.setInformacion(dato);
							p.setEnlaceD(nuevo);
						}else{
							insertar(p.getEnlaceD(), dato);
						}
					}
				}
			}else System.out.println("El abol no esta creado");
		} catch (Exception e) {
			System.err.println("Ocurrio un error tratando de insertar un nodo");
		}
	}
	
	private void buscar(Nodo nodo,int dato){
		try {
			if (dato == nodo.getInformacion()){
				System.out.println("Encontrado");
			}
			else
			    if (dato > nodo.getInformacion())
			        buscar(nodo.getEnlaceD(), dato);
			    else
			    	buscar(nodo.getEnlaceI(), dato);
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}//end-buscar
	
	private void eliminar(Nodo p, int dato){
		try {
			if(p == null){ System.out.println("La clave no se encuentra en el arbol"); }
			else{
				if(dato < p.getInformacion()){
					q = p;
					eliminar(p.getEnlaceI(), dato); //Recursividad
				}else{
					if(dato > p.getInformacion()){	q = p;	eliminar(p.getEnlaceD(), dato);	} //Recursividad
					else{
						otro = p;
						if(otro.getEnlaceI() == null && otro.getEnlaceD() == null){
							if(otro == raiz) raiz = null;
							else{
								if(q.getEnlaceI() == p) q.setEnlaceI(p.getEnlaceI());
								else{
									if(q.getEnlaceD() == p) q.setEnlaceD(p.getEnlaceD());
								}
							}
						}//if-bisnieto
						else{
							if(otro.getEnlaceD() == null){
								if(otro == raiz){
									raiz = otro.getEnlaceI();
								}else{
									p = otro.getEnlaceI();
									otro.setInformacion(p.getInformacion());
									otro.setEnlaceI(p.getEnlaceI());
									otro.setEnlaceD(p.getEnlaceD());
									otro = p;
								}
							}else{
								if(otro.getEnlaceI() == null){
									if(otro == raiz) raiz = otro.getEnlaceD();
									else{
									p = otro.getEnlaceD();
									otro.setInformacion(p.getInformacion());
									otro.setEnlaceD(p.getEnlaceD());
									otro.setEnlaceI(p.getEnlaceI());
									otro = p;
									}
								}else{
									q = otro.getEnlaceI();
									r = q;
									while(q.getEnlaceD() != null){
										r = q;
										q  = q.getEnlaceD();
									}//end-while
									otro.setInformacion(q.getInformacion());
									otro = q; 
									r.setEnlaceD(q.getEnlaceI());
									if(p.getEnlaceI() == otro) p.setEnlaceI(otro.getEnlaceI());
									else if (p.getEnlaceD() == otro) p.setEnlaceD(otro.getEnlaceD());
								}
							}
						}//if-else-bisnieto
						otro = null; p = null; q = null; r = null; //Librar memoria
						System.out.println("Eliminado correctamente.");
					}//else-nieto
				}//else-hijo
			}//else-padre
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}//end-eliminar
	//end-operaciones basicas
	
	//Ordenamiento
	public void ordenamiento(int ID){
		try {
			switch(ID){
				case 1: preOrden(raiz);
			break;
				case 2: inOrden(raiz);
			break;
				case 3: posOrden(raiz);
			break;
			}
		} catch (Exception e) {
			System.out.println("Hubo un error elijiendo el ordenamiento.");
		}
	}
	
	private void preOrden(Nodo p){
		try {
			if(p != null){
				System.out.println(p.getInformacion());
				nodosItemp += 1;
				if(nodosItemp > nivelI && p.getEnlaceI() != null) nivelI = nodosItemp;
				preOrden(p.getEnlaceI());
				preOrden(p.getEnlaceD());
				nodosDtemp += 1;
				if(nodosDtemp > nivelD && p.getEnlaceD() != null) nivelD = nodosDtemp;
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
		System.out.println("I : "+nivelI+" D : "+nivelD);
		nodosItemp = 1; nodosDtemp = 1;//limpio toda informacion
	}

	private void inOrden(Nodo p){
		try {
			if(p != null){
				inOrden(p.getEnlaceI());
				System.out.println(p.getInformacion());
				inOrden(p.getEnlaceD());
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}

	private void posOrden(Nodo p){
		try {
			if(p != null){
				posOrden(p.getEnlaceI());
				posOrden(p.getEnlaceD());
				System.out.println(p.getInformacion());
			}
		} catch (Exception e) {
			System.err.println("Error, no puedo mostrar nada.");
		}
	}
	//end-ordenamientos

	//operaciones extra
	public boolean isEmpty(){
		if(raiz == null) return true;
		else return false;
	}
	
	public int getNivelMayor(){
		if(nivelI > nivelD)	return nivelI;
		else return nivelD;
	}
	
	public int getNivelI(){
		return nivelI;
	}
	
	public int getNivelD(){
		return nivelD;
	}
	//operaciones de examen
}//class