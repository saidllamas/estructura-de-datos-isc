package com.arbolesmejorados.saidllamas;

public class Nodo {
	
	private int informacion;
	private Nodo enlaceD, enlaceI;
	private int nivel;
	
	public Nodo(){
		informacion = 0;
		nivel = -1; //negativo para identificar posibles errores
		enlaceD = null;
		enlaceI = null;
	}
	
	public void setNivel(int nivel){
		this.nivel = nivel;
	}
	
	public int getNivel(){
		return nivel;
	}

	public int getInformacion() {
		return informacion;
	}

	public void setInformacion(int informacion) {
		this.informacion = informacion;
	}

	public Nodo getEnlaceD() {
		return enlaceD;
	}

	public void setEnlaceD(Nodo enlaceD) {
		this.enlaceD = enlaceD;
	}

	public Nodo getEnlaceI() {
		return enlaceI;
	}

	public void setEnlaceI(Nodo enlaceI) {
		this.enlaceI = enlaceI;
	}
}