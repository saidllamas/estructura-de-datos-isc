package com.saidllamas.diagnostico;

public class Main {

	public static void main(String[] args) {
		int numeros[] = new int[100];
		for (int i = 0; i <= 99; i++) {
			numeros[i] = (i+1) * (i+1);
		}
		for (int i = 0; i <= 99; i++) {
			System.out.println("Numero: "+(i+1)+", cuadrado = "+numeros[i]);
		}
	}

}
