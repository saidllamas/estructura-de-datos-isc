package com.saidllamas.diagnostico;

import java.util.Scanner;

public class Cadenas {

	public static void main(String[] args) {
		String cadena1, cadena2,concatenar = "";
		Scanner entrada = new Scanner(System.in);
		System.out.println("Cadena 1");
		cadena1 = entrada.nextLine();
		System.out.println("Cadena 2");
		cadena2 = entrada.nextLine();
		if(cadena1.compareTo(cadena2)==0){
			System.out.println("Iguales");
		}
		else if(cadena1.compareTo(cadena2)<0){
			concatenar = cadena1.concat(cadena2);
		}
		else{ 
			concatenar = cadena2.concat(cadena1);
		}
		System.out.println(concatenar+", caracteres: "+concatenar.length());
	}

}
