package com.ordenamientos.saidllamas;

import java.util.Scanner;

public class Main {
	
	static int[] arregloOriginal;
	static int ini, fin, pos;

	public static void main(String[] args){
		try {
			System.out.println("¡Bienvenido!");
			menu();
		} catch (Exception e) {
			System.out.println("Error inesperado en el codigo. : "+e);
		}
	}
	
	static void menu(){
		int opc = -1;
		do{
			System.out.println("MENÚ");
			System.out.println("Elija operacion: ");
			System.out.println("1) Generar valores ");
			System.out.println("2) Burbuja ");
			System.out.println("3) Quicksort ");
			System.out.println("4) Shellsort ");
			System.out.println("5) Búsqueda secuencial ");
			System.out.println("6) Búsqueda binaria ");
			System.out.println("7) Salir. ");
			opc = recibirDato(); //mando llamar a la entrada
			if(opc > 0 && opc < 7) controlador(opc);
			else if(opc == -1) System.out.print(""); //opcion default-cuando entra una letra
			else if(opc == 7){
				System.out.println("Fin del programa.");
				break;
			}
			else{
				System.err.println("Ingrese una opcion desde el 1 - 7");
			}
		}while(opc != 7);
	}
	
	static void controlador(int ID){
		switch (ID) {
		case 1:
			System.out.println("1) Generar valores.");
			generarValores();
			break;
		case 2:
			System.out.println("2) Burbuja ");
			if(!isEmpty()){
				if(!burbuja()) printArreglo();
			}
			break;
		case 3:
			System.out.println("3) Quicksort ");
			if(!isEmpty()){
				if(!quicksort()) printArreglo();
			}
			break;
		case 4:
			System.out.println("4) Shellsort ");
			if(!isEmpty()){
				if(!shellsort()) printArreglo();
			}
			break;
		case 5:
			System.out.println("5) Búsqueda secuencial ");
			if(!isEmpty()){
				busquedaSecuencial();
			}
			break;
		case 6:
			System.out.println("6) Búsqueda binaria ");
			if(!isEmpty()){
				busquedaBinaria();
			}
			break;
		} 
	}
	
	//validaciones
		static int recibirDato(){
			int dato = -1;
			do{
				try {
					System.out.println("|..");
					Scanner entrada = new Scanner(System.in);
					dato = entrada.nextInt();
				} catch (Exception e) {
					System.out.println("Ocurrio un error en la entrada. : "+e);
				}
			}while(dato < 1 | dato > 100);
			return dato;
		}
		
		static boolean isEmpty(){
			try {
				int t = arregloOriginal[0];
				return false;
			} catch (Exception e) {
				System.out.println("Ocurrio un error con el arreglo: " +e.getMessage());
				return true;
			}
		}
		
	//Operaciones
	static void generarValores(){
		int tamaño = -1;
		do{
			System.out.println("Ingrese tamaño del arreglo: ");
			tamaño = recibirDato();
		}while(tamaño < 2 || tamaño > 10);
		crearArreglo(tamaño);
		for(int i = 0; i < tamaño; i++){
			int numero = (int) (Math.random() * 99) + 1;
			insertarArreglo(i,numero);
		}
		System.out.print("Valores generados: ");
		printArreglo();
	}
	
	static void crearArreglo(int n){
		try {
			arregloOriginal = new int[n];
			System.out.println("Creado exitosamente.");
		} catch (Exception e) {
			System.out.println("Ocurrio un error al crear el arreglo. : "+e);
		}
	}
	
	static void insertarArreglo(int pos, int numero){
		try {
			arregloOriginal[pos] = numero;
		} catch (Exception e) {
			System.out.println("Ocurrio un error al insertar. : "+e);
		}
	}
	
	//ordenamientos
	
	static boolean burbuja(){
		boolean isOrdenado = false;
		try {
			int pasadas = 1;
			int t = arregloOriginal.length - 1;
			for(int i = 0; i < t; i++){
				for(int j = 0; j < t; j++){
					if(arregloOriginal[j] > arregloOriginal[j+1]){
						System.out.println("Pasada: "+pasadas++);
						int aux = arregloOriginal[j+1];
						printArreglo();
						arregloOriginal[j+1]=arregloOriginal[j];
						arregloOriginal[j] = aux;
						printArreglo();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Ocurrio un error al tratar de ordenar. : "+e); 
		}
		return isOrdenado;
	}
	
	static boolean quicksort(){
		boolean isOrdenado = false;
		try {
			int pasadas = 1;
			int top = 1, pilaMenor[] = new int[10], pilaMayor[] = new int[10];
			pilaMenor[top] = 0;
			pilaMayor[top] = arregloOriginal.length-1;
			while(top > 0){
				ini = pilaMenor[top];
				fin = pilaMayor[top];
				top = top -1;
				pasadas = ordena(pasadas);
				isOrdenado = true;
				if(ini < pos-1){
					top = top +1;
					pilaMenor[top] = ini;
					pilaMayor[top] = pos -1;
				}
				if(fin > pos+1){
					top = top +1;
					pilaMenor[top] = pos+1;
					pilaMayor[top] = fin;
				}
			}
		} catch (Exception e) {
			System.out.println("Ocurrio un error al tratar de ordenar. : "+e);
		}
		return isOrdenado;
	}
	
	static int ordena(int pasadas){
		try {
			int izq, der, aux = 0;
			boolean band;
			izq = ini; der = fin; pos = ini; band = true;
			while(band){
				while(arregloOriginal[pos] <= arregloOriginal[der] & pos != der){ der = der -1;}
				if(pos == der) band = false;
				else{
					System.out.println("Pasada: "+pasadas++);
					aux = arregloOriginal[pos];
					printArreglo();
					arregloOriginal[pos] = arregloOriginal[der];
					arregloOriginal[der] = aux;
					printArreglo();
					pos = der;
					while(arregloOriginal[pos] >= arregloOriginal[izq] & pos != izq){ izq = izq+1;}
					if(pos == izq) band = false;
					else{
						aux = arregloOriginal[pos];
						arregloOriginal[pos] = arregloOriginal[izq];
						arregloOriginal[izq] = aux;
						pos = izq;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Ocurrio un error inesperado ordenando. : "+e);
		}
		return pasadas;
	}
	
	static boolean shellsort(){
		boolean isOrdenado = false;
		try {
			int pasadas = 1;
			int in = arregloOriginal.length + 1, i, aux;	
			boolean band;
			while(in > 1){
				in = (int) in / 2;
				band = true;
				while(band){
					band = false;
					i = 0;
					while((i + in) < arregloOriginal.length){
						if(arregloOriginal[i] > arregloOriginal[i+in]){
							System.out.println("Pasada: "+pasadas++);
							aux = arregloOriginal[i];
							printArreglo();
							arregloOriginal[i] = arregloOriginal[i+in];
							arregloOriginal[i+in] = aux;
							printArreglo();
							band = true;
							isOrdenado = true;
						}
						i++;	
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Ocurrio un error al tratar de ordenarlos. : "+e);
		}
		return isOrdenado;
	}
	
	//Busquedas
	
	static void busquedaSecuencial(){
		try {
			int numero = -1;
			do{
				System.out.println("Ingrese numero a buscar: ");
				numero = recibirDato();
			}while(numero < 1 || numero > 100);
			int i = 0, bandera = 0;
			while(i < arregloOriginal.length && bandera == 0){
				if(numero == arregloOriginal[i]) bandera = 1;
				else i++;
			}
			if(bandera == 1) System.out.println("El elemento esta en la posicion I:"+(i+1));
			else System.out.println("El elemento no esta en el arreglo");
		} catch (Exception e) {
			System.out.println("Ocurrio algun error inesperado dentro de la busqueda. : "+e);
		}
	}
	
	static void busquedaBinaria(){
		try {
			int centro =0;
			burbuja();
			System.out.println("Ordenado correctamente.");
			int numero = -1;
			do{
				System.out.println("Ingrese numero a buscar: ");
				numero = recibirDato();
			}while(numero < 1 || numero > 100);
			int izq = 0, der = (arregloOriginal.length - 1) , bandera = 0;
			while(izq <= der && bandera == 0){
				centro = (int)(izq + der) / 2;
				if(numero == arregloOriginal[centro]) bandera = 1;
				else if(numero > arregloOriginal[centro]) izq = centro+1;
				else der = centro-1;
			}
			if(bandera == 1) System.out.println("El elemento esta en la posicion CENTRO: "+(centro+1));
			else System.out.println("El elemento no esta en el arreglo");
		} catch (Exception e) {
			System.out.println("Ocurrio algun error inesperado dentro de la busqueda. : "+e);
		}
	}
	
	static void printArreglo(){
		try {
			for(int i = 0; i < arregloOriginal.length; i++){
				if(i+1 == arregloOriginal.length) System.out.print(arregloOriginal[i]+".");
				else System.out.print(arregloOriginal[i]+", ");
			}
			System.out.println("");
		} catch (Exception e) {
			System.out.println("Error al tratar de imprimir. :"+e);
		}
	}
}