package com.saidllamas14.examen2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int n = entrada();
		System.out.println("Iterativo: "+iterativo(n));
		System.out.println("Recursivo: "+ recursivo(n));
	}
	
	private static long iterativo(int n){
		int res = 0;
		for (int i = n; i >= 1; i--) {
			res += i;
		}
		return res;
	}
	
	private static long recursivo(int n){
		if(n==1)return 1;
		else{ return n  += recursivo(n-1); }
	}
	
	private static int entrada(){
		int n = -1;
		do{
			Scanner entrada = new Scanner(System.in);
			System.out.println("Ingrese n");
			n = entrada.nextInt();
		}while(n < 0);
		if(n==0){
			System.out.println("Iterativo: "+0);
			System.out.println("Recursivo: "+0);
			System.exit(0);
		}
		return n;
	}

}