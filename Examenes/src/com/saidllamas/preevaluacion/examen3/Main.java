package com.saidllamas.preevaluacion.examen3;

import java.util.Scanner;

public class Main {
	
	static Scanner entrada = new Scanner(System.in); //scanner universal
	static Pila pila = new Pila(); //para programa 1
	
	//prioridad de los operarodes en numero entero
	private static final int P_SUMA = 1;
	private static final int P_RESTA = 1;
	private static final int P_MULTIPLICACION = 2;
	private static final int P_DIVISION = 2;
	private static final int P_POTENCIA = 3; 
	
	public static void main(String[] args) {
		System.out.println("¡Bienvenido/a!");
		int opc = 0;
		do{
			opc = menu();
			if(opc == 1) 		addPila();
			else if(opc == 2)	removePila();
			else if(opc == 3)   generateEI();
			else if(opc == 5)	examen();
			else if(opc == 4){
				System.out.println("Fin del programa.");
				break;
			} else System.out.println("Opcion invalida.");
		}while(opc != 4);
	}
	

	private static int menu(){
		int opc = 0;
		System.out.println("Elija una opcion: ");
		System.out.println("1) Añadir a pila.");
		System.out.println("2) Quitar de pila.");
		System.out.println("3) Convertir una expresion.");
		System.out.println("5) Examen");
		System.out.println("[4] Terminar.");
		try {
			Scanner en = new Scanner(System.in);
			opc = en.nextInt();
		} catch (Exception e) {
			System.out.println("Ocurrio un error.");
		}
		return opc;
	}
	
	private static void examen() {
		System.out.println(pila.examen());
	}
	
	private static void addPila(){
		boolean isNumber = true;
		int number = -1;
		do{
			Scanner temp = new Scanner(System.in);
			System.out.println("Ingrese un numero entero");
			try{
				number = temp.nextInt();
			}catch (Exception e) {
				System.out.println("Ocurrio un error con la entrada...");
				isNumber = false;
			}finally {
				if(number>0) isNumber = true;
			}
			
		}while(!isNumber);
		if(number < 1 | number > 100) addPila();
		else{
			pila.push(number);
			pila.imprimirArregloCompleto();
		}
	}
	
	private static void removePila(){
		pila.pop();
		pila.update();
	}
	
	private static void generateEI(){
		boolean ei = false;
		String EI = "";
		do{ //primero valido los elementos
			System.out.println("Ingrese expresion");
			EI = entrada.nextLine();
			for (int i = 0; i < EI.length(); i++) {
				//recorre el entrada justo despues de leerla, si hay error la solicita de nuevo
				ei = validarExpresionMatematica(EI.charAt(i));
				if(ei == false) break; 
			}
		}while(!ei);
		boolean parentesis = validarExpresionMatematica(EI); //compruebo que se tengan la misma cantidad de ( con )
		boolean sumas = validarExpresionMatematica(EI, '+'); //compruebo el problema con +
		boolean restas = validarExpresionMatematica(EI, '-'); //compruebo el problema con -
		boolean divisiones = validarExpresionMatematica(EI, '/'); //compruebo el problema con /
		boolean multiplicaciones = validarExpresionMatematica(EI, '*'); //compruebo el problema con *
		boolean potencias = validarExpresionMatematica(EI, '^'); //compruebo el problema con ^
		if(parentesis | sumas | restas | divisiones | multiplicaciones | potencias){
			System.out.println("Ocurrio un error con su expresion, hay dos operadores seguidos o un parentesis de mas, intentelo de nuevo");
			menu();
		}	else procesarExpresion(EI);
	}
	
	private static boolean validarExpresionMatematica(char c){
		//operadores matematicos
		if(c == '(' | c == ')') return true;
		else if(c == '+' | c == '-' | c == '*' | c == '/' | c == '^' ) return true;
		//letras
		else if(c == 'a' | c == 'A' | c == 'b' | c == 'B' | c == 'c' | c == 'C' | c == 'd' | c == 'D' | c == 'e' | c == 'E' | c == 'f' | c == 'F' | c == 'g' | c == 'G' | c == 'h' | c == 'H' | c == 'i' | c == 'I' | c == 'j' | c == 'J' | c == 'k' | c == 'K' | c == 'l' | c == 'L' | c == 'm' | c == 'M' | c == 'n' | c == 'N' | c == 'ñ' | c == 'Ñ' | c == 'o' | c == 'O' | c == 'p' | c == 'P' | c == 'q' | c == 'Q' | c == 'r' | c == 'R' | c == 's' | c == 'S' | c == 't' | c == 'T' | c == 'u' | c == 'U' | c == 'v' | c == 'V' | c == 'w' | c == 'W' | c == 'x' | c == 'X' | c == 'y' | c == 'Y' | c == 'z' | c == 'Z') return true;
		//numeros
		else if(c == '0' | c == '1' | c == '2' | c == '3' | c == '4' | c == '5' | c == '6' | c == '7' | c == '8' | c == '9') return true;
		//caracter no permitido
		else return false;
	}
	
	private static boolean validarExpresionMatematica(String EI){
		int parentesisIz = 0, parentesisDer = 0;
		for(int i = 0; i < EI.length(); i++){
			/*if(EI.charAt(0)==')'){
				System.out.println("Error, expresion inicia con )");
				menu();
				break;
			}*/
			if(EI.charAt(i) == '(') parentesisIz++;
			else if(EI.charAt(i) == ')') parentesisDer++;
		}
		if(parentesisIz != parentesisDer) return true;
		else return false;
	}
	
	private static boolean validarExpresionMatematica(String EI, char c){
		boolean error = false;
		for(int i = 0; i < EI.length()-1; i++){
			if(EI.charAt(i) == '+' | EI.charAt(i) == '-' | EI.charAt(i) == '*' | EI.charAt(i) == '/' | EI.charAt(i) == '^'){
				if(EI.charAt(i) == EI.charAt(i+1)){
					error = true;
					break;
				}
			}
		}
		return error;
	}
	
	
	private static void procesarExpresion(String cadena){
		char[] operadores = new char[10]; //pila para operadores
		String EP = "";  //variable que almacena la expresion transformada
		int tope = 0; //tope inicial
		
		//recorrer string
		for(int i = 0; i < cadena.length(); i++){
			if(cadena.charAt(i) == '('){
				tope++; //a pila
				operadores[tope] = cadena.charAt(i); //asigno valor a pila
				System.out.println("Tope: "+tope+" ["+operadores[tope]+"]");
			}else{
				//si simbolo )
				if(cadena.charAt(i) == ')'){
					for(int o = tope; o > 0; o--){
						if(operadores[o] == '(') break;
						else {
							EP += operadores[o]; //mando imprimir el operador
							tope--; //saco operador
							System.out.println("Tope: "+tope+" ["+operadores[tope]+"]");
						}
					}
					tope--; //elimino parentesis iz de pila
					System.out.println("Tope: "+tope+" ["+operadores[tope]+"]");
				}else{
					if(cadena.charAt(i) == '+' | cadena.charAt(i) == '-' | cadena.charAt(i) == '*' | cadena.charAt(i) == '/' | cadena.charAt(i) == '^'){
						if(tope > 0 && getPrioridad(cadena.charAt(i)) <= getPrioridad(operadores[tope])){
							System.out.println("Prioridad Cadena: "+cadena.charAt(i)+" : "+getPrioridad(cadena.charAt(i))+" <= Prioridad Pila:"+operadores[tope]+" : "+getPrioridad(operadores[tope]));
							EP += operadores[tope]; //imprimo operador
							tope--; //saco operador
							System.out.println("Tope: "+tope+" ["+operadores[tope]+"]");
						}
						tope++; //añado operador
						operadores[tope] = cadena.charAt(i); //asigno valor
						System.out.println("Tope: "+tope+" ["+operadores[tope]+"]");
					}else{
						EP += cadena.charAt(i); //es operando, mando a imprimir directamente
					}
				}
			}
		}
		System.out.println("Vaciando pila");
		//vacia pila de operadores
		for(int i = tope; i > 0; i--){
			EP += operadores[i];		
			System.out.println("Tope: "+i+" ["+operadores[i]+"]");
		}
		System.out.println("Expresion posfija: "+EP); //impresion final
		System.out.println("");
	}
	
	private static int getPrioridad(char c){
		if(c == '+') return P_SUMA;
		else if(c == '-') return P_RESTA;
		else if(c == '*') return P_MULTIPLICACION;
		else if(c == '/') return P_DIVISION;
		else if(c == '^') return P_POTENCIA;
		else return 0; //para los '(' y ')'
	}
	

}
