package com.saidllamas.preevaluacion.examen3;

public class Pila {
	
	static int[] pila = new int[5]; //contenedor
	static int tope = 0; //tope inicial
		
	public void push(int n){
		if(tope < pila.length){ //si no esta lleno
			pila[tope] = n; //asigno valor
			tope++; //pila ocupado +1
		}else System.out.println("¡Desbordamiento! No agregado.");
		
	}
	
	
	public void pop(){
		if(tope > 0){
			tope--; //pila -1 lugar
			System.out.println("Eliminado correctamente");
		} else System.out.println("¡Subdesbordamiento! No hay que eliminar.");
	}
	
	public Boolean empty(){
		if(tope == 0) return true; //no hay posiciones ocupadas
		else return false; //hay posiciones ocupadas
	}
	
	public void imprimirArregloCompleto(){
		int topeReal = tope; //valor original
		for(int i = tope-1; i >= 0 ; i--){
			if(pila[i] !=0 )System.out.println(tope-- +" ["+pila[i]+"]");//si la casilla tiene un valor
		}
		tope = topeReal; //regreso su valor original
	}
	
	public void update(){
		int topeReal = tope; //valor original
		for (int i = tope-1; i >= 0 ; i--) {
			if(pila[i] !=0 )System.out.println(tope-- +" ["+pila[i]+"]"); //si la casilla tiene un valor
		}
		tope = topeReal; //le regreso su valor
	}
	
	public String examen(){
		String msj = "";
		if(tope > 0){
			if(pila[tope-1] > 0 && pila[tope-1]<= 25){
				msj = "Entre el 1-25";
			}
			else if(pila[tope-1] > 25 && pila[tope-1]<= 50){
				msj = "Entre el 26-50";
			}
			else if(pila[tope-1] > 50 && pila[tope-1]<= 75){
				msj = "Entre el 50-75";
			}
			else if(pila[tope-1] > 75 && pila[tope-1]<= 100){
				msj = "Entre el 75-100";
			}
			System.out.println("Eliminado correctamente");
			tope--;
		} else System.out.println("¡Subdesbordamiento! No hay que eliminar.");
		return msj;
		
	}
}
