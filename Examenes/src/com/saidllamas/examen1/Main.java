package com.saidllamas.examen1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int matriz[] = new int[9];
		Scanner entrada = new Scanner(System.in);
		for (int i = 0; i < matriz.length; i++) {
			System.out.println("Ingrese valor en: "+(i+1));
			matriz[i] = entrada.nextInt();
		}
		
		for (int i = 0; i < matriz.length; i++) {
			System.out.print(matriz[i]+"  ");
			if(i==2||i==5) System.out.println("");
		}
		int suma1 = 0, suma2 = 0, suma3 = 0;
		for(int i = 0; i < matriz.length; i+=3){
			suma1 += matriz[i];
		}
		for(int i = 1; i < matriz.length; i+=3){
			suma2 += matriz[i];
		}
		for(int i = 2; i < matriz.length; i+=3){
			suma3 += matriz[i];
		}
		System.out.println("");
		System.out.println("--------");
		System.out.print(suma1+" ");
		System.out.print(suma2+" ");
		System.out.print(suma3);
	}

}
