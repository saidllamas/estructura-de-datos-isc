package com.saidllamas.ec.jose;

import java.util.Scanner;

public class Main {
	
	private static Lista lista = new Lista();

	public static void main(String[] args) {
		System.out.println("¡Bienvenido/a!");
		System.out.println("Usted esta asesinando a soldados inocentes.");
		entradas();
	}
	
	private static void entradas(){
		int nPersonas = -1, limite = -1;
		do{
			System.out.println("Ingrese numero de soldados ");
			try {
				Scanner en = new Scanner(System.in);
				nPersonas = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}	
		}while(nPersonas < 0);
		do{
			System.out.println("Ingrese cada cuanto se asesina a un soldado");
			try {
				Scanner en = new Scanner(System.in);
				limite = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}	
		}while(limite < 0);
		for(int i = 0; i < nPersonas; i++){
			//añadir nodo con informacion proveniente de i
			lista.insertar(i);
		}
		lista.consultar();
	}
}
