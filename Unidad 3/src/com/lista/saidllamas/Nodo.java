package com.lista.saidllamas;

public class Nodo {
	
	private int informacion;
	private Nodo enlace;
	
	public Nodo(){
        this.informacion = 0;
        this.enlace = null;
    }
	
	public int getInformacion() {
		return informacion;
	}
	public void setInformacion(int informacion) {
		this.informacion = informacion;
	}
	public Nodo getEnlace() {
		return enlace;
	}
	public void setEnlace(Nodo enlace) {
		this.enlace = enlace;
	}

}