package com.lista.saidllamas;


public class Lista {
	
	private Nodo inicio, aux, q, n;
	
	public void creaFinal(int val){
		Nodo nodo = new Nodo();
		nodo.setInformacion(val);
		nodo.setEnlace(null);
		if(inicio == null){
			inicio = nodo;
		}
		else{
			aux.setEnlace(nodo); 
		}
		aux = nodo;
	}
	
	public void consultar(){
		q = inicio;
		if(inicio == null) System.out.println("Lista vacía.");
		while(q != null){
			System.out.print("["+q.getInformacion()+" | "+q.getEnlace()+"]");
			System.out.print(" -> ");
			q = q.getEnlace();
		}
		System.out.println("");
	}
	
	public void consultar(int dato){
		String msj = "No encontrado";
		q = inicio;
		while(q != null){
			if(q.getInformacion()==dato) msj = "Encontrado";
			q = q.getEnlace();
		}
		System.out.println(msj);
	}
	
	public void consultarExamen(){
		q = inicio;
		int temp;
		if(inicio == null) System.out.println("Lista vacía.");
		while(q != null){
			q = q.getEnlace();
			temp = q.getInformacion();
			if(q.getEnlace() == null){
				System.out.println(temp);
				break;
			}
		}
	}
	
	public void borrar(int val){
		if(inicio == null) System.out.println("Lista vacía.");
		else{
			int ban = buscar(val);
			if(ban == 1){
				if(n == inicio)
					inicio = inicio.getEnlace();
				else q.setEnlace(n.getEnlace());
				if(n.getEnlace() == null) aux = q;
				n = null;
				System.out.println("Eliminado correctamente.");
			}
		}
	}
	
	public int buscar(int val){
		n = inicio;
		int ban = 0;
		while(n != null && ban == 0){
			if(n.getInformacion() == val){
				ban = 1;
			}
			else {
				q = n;
				n = n.getEnlace();
			}
		}
		if(ban == 0) System.out.println("Este dato no existe.");
		return ban;
	}
}
