package com.lista.saidllamas;

import java.util.Scanner;

public class Main {
	
	private static Lista lista = new Lista();

	public static void main(String[] args) {
		System.out.println("¡Bienvenido/a!");
		int opc = 0;
		do{
			opc = menu();
			if(opc == 1) 		insertar();
			else if(opc == 2)	eliminar();
			else if(opc == 3) 	consultar();
			else if(opc == 4) 	consultarIndividual();
			else if(opc == 5) 	examen();
			else if(opc == 6){
				System.out.println("Fin del programa.");
				break;
			} else System.out.println("Opcion invalida.");
		}while(opc != 6);
	}
	
	private static int menu(){
		int opc = 0;
		System.out.println("Elija una opcion: ");
		System.out.println("1) Insertar.");
		System.out.println("2) Eliminar.");
		System.out.println("3) Consultar ");
		System.out.println("4) Buscar (individual)");
		System.out.println("5) Examen");
		System.out.println("[6] Terminar.");
		try {
			Scanner en = new Scanner(System.in);
			opc = en.nextInt();
		} catch (Exception e) {
			System.out.println("Ocurrio un error.");
		}
		return opc;
	}
	
	private static void insertar(){
		int val = -1;
		do{
			System.out.println("Ingrese un numero 1-100");
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		if(val > 100 || val < 1) insertar();
		else{
			lista.creaFinal(val);
			System.out.println("Insertado correctamente.");
		}
	}
	
	private static void consultar(){
		lista.consultar();
	}
	
	private static void eliminar(){
		System.out.println("Dato a eliminar");
		int val = -1;
		do{
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		lista.borrar(val);
	}
	
	private static void consultarIndividual(){
		System.out.println("Dato a buscar");
		int val = -1;
		do{
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		lista.consultar(val);
	}
	
	private static void examen(){
		lista.consultarExamen();
	}
}
