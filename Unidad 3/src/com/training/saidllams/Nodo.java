package com.training.saidllams;

public class Nodo {
	
	private int informacion;
	private Nodo enlaceD, enlaceI;
	
	public Nodo(){
		informacion = 0;
		enlaceD = null;
		enlaceI = null;
	}

	public int getInformacion() {
		return informacion;
	}

	public void setInformacion(int informacion) {
		this.informacion = informacion;
	}

	public Nodo getEnlaceD() {
		return enlaceD;
	}

	public void setEnlaceD(Nodo enlaceD) {
		this.enlaceD = enlaceD;
	}

	public Nodo getEnlaceI() {
		return enlaceI;
	}

	public void setEnlaceI(Nodo enlaceI) {
		this.enlaceI = enlaceI;
	}
	
}
