package com.training.saidllams;

import java.util.Scanner;

public class Main {
	
	private static Lista lista = new Lista();

	public static void main(String[] args) {
		System.out.println("¡Bienvenido/a!");
		int opc = 0;
		do{
			opc = menu();
			if(opc == 1) 		insertar();
			else if(opc == 2) 	insertarReferencia();
			else if(opc == 3)	eliminar();
			else if(opc == 4) 	consultar();
			else if(opc == 5){
				System.out.println("Fin del programa.");
				break;
			} else System.out.println("Opcion invalida.");
		}while(opc != 5);
	}
	
	private static int menu(){
		int opc = 0;
		System.out.println("Elija una opcion: ");
		System.out.println("1) Insertar.");
		System.out.println("2) Insertar Por Referencia.");
		System.out.println("3) Eliminar.");
		System.out.println("4) Consultar ");
		System.out.println("[5] Terminar.");
		try {
			Scanner en = new Scanner(System.in);
			opc = en.nextInt();
		} catch (Exception e) {
			System.out.println("Ocurrio un error.");
		}
		return opc;
	}
	
	private static void insertar(){
		int val = -1;
		do{
			System.out.println("Ingrese un numero 1-100");
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		if(val > 100 || val < 1) insertar();
		else{
			lista.insertar(val);
		}
	}
	
	private static void insertarReferencia(){
		int val = -1;
		int idInsertar = -1;
		boolean isCorrect = false, isEmpty = false;
		do{
			System.out.println("Ingrese un numero 1-100");
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		if(val > 100 || val < 1){
			insertar();
		}
		else{
			do{
				if(lista.consultarEspecial()){
					System.out.println("¿Despues de que elemento desea insertarlo? \n Ingrese ID []");
					do{
						try {
							Scanner en = new Scanner(System.in);
							idInsertar = en.nextInt();
						} catch (Exception e) {
							System.out.println("Ocurrio un error.");
						}
					}while(idInsertar == -1);
				} else{
					idInsertar = -2;
					System.out.println("Lista vacia, no se puede insertar por referencia");
					isEmpty = true;
				}
			}while(idInsertar == -1);
			//System.out.println("ID elegido: "+idInsertar);
			if(idInsertar > 0) isCorrect = lista.insertarEspecial(val, idInsertar);
		}
	}
	
	private static void consultar(){
		lista.consultar();
	}
	
	private static void eliminar(){
		System.out.println("Dato a eliminar");
		int val = -1;
		do{
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		lista.borrar(val);
	}

}
