package com.training.saidllams;

import java.util.Scanner;

public class Lista {
	
	private Nodo inicio, fin, aux, n, q;
	private int nodos;
	
	public Lista(){
		nodos = 0;
	}
	
	public void insertar(int val){
		Nodo nodo = new Nodo();
		nodo.setInformacion(val);
		nodo.setEnlaceI(null);
		nodo.setEnlaceD(null);
		if(inicio == null){
			inicio = nodo;
			fin = nodo;
			System.out.println("Agregado correctamente");
		}else{
			int opc = -1;
			do{
				System.out.println("¿Por frente o por final? \n 1) Frente 2) Final");
				try {
					Scanner entrada = new Scanner(System.in);
					opc = entrada.nextInt();
				} catch (Exception e) {
					System.out.println("Ocurrio un error.");
				}
			}while(opc < 1 && opc > 2);
			if(opc == 1){
				inicio.setEnlaceI(nodo);
				nodo.setEnlaceD(inicio);
				inicio = nodo;
			} else if(opc == 2){
				fin.setEnlaceD(nodo);
				nodo.setEnlaceI(fin);
				fin = nodo;
			}
			System.out.println("Agregado correctamente");
		}
		nodos++;
		System.out.println("Nodos creados: "+nodos);
	}
	
	public boolean insertarEspecial(int val, int pos){
		Nodo nodo = new Nodo();
		boolean isCorrect = false;
		for(int i = 0; i <= nodos; i++){
			nodo.getEnlaceD();
			if(pos == i){
				System.out.println("Encontrado.");
				isCorrect = true;
			}
		}//NO OLVIDAR nodos++;
		if(isCorrect){
			nodo.setInformacion(val);
			nodo.setEnlaceI(null);
			nodo.setEnlaceD(null);
			if(inicio == null){
				inicio = nodo;
				fin = nodo;
				System.out.println("Agregado correctamente");
			}else{
				int opc = -1;
				do{
					System.out.println("¿Por frente o por final? \n 1) Frente 2) Final");
					try {
						Scanner entrada = new Scanner(System.in);
						opc = entrada.nextInt();
					} catch (Exception e) {
						System.out.println("Ocurrio un error.");
					}
				}while(opc < 1 && opc > 2);
				if(opc == 1){
					inicio.setEnlaceI(nodo);
					nodo.setEnlaceD(inicio);
					inicio = nodo;
					System.out.println("Agregado correctamente");
				} else if(opc == 2){
					fin.setEnlaceD(nodo);
					nodo.setEnlaceI(fin);
					fin = nodo;
					System.out.println("Agregado correctamente");
				}			
			}
			nodos++;
			System.out.println("Nodos creados: "+nodos);
		}//isCorrect
		return isCorrect;
	}
	
	public void consultar(){
		int opc = -1;
		do{
			System.out.println("¿Consultar por frente o por final? \n 1) Frente 2) Final");
			try {
				Scanner entrada = new Scanner(System.in);
				opc = entrada.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(opc < 1 && opc > 2);
		if(opc == 1) aux = inicio;
		else if(opc == 2) aux = fin;
		while(aux != null){
			System.out.print("[ " + aux.getEnlaceI() + " | " + aux.getInformacion() + " | " + aux.getEnlaceD() + " ]");
			System.out.print(" <-> ");
			if(opc == 1)
				aux = aux.getEnlaceD();
			else aux = aux.getEnlaceI();
		}
		System.out.println("");
	}
	
	public boolean consultarEspecial(){
		boolean hayDato = false;
		aux = inicio;
		int id = 0;
		while(aux != null){
			System.out.print("ID :" +id + " [ " + aux.getEnlaceI() + " | " + aux.getInformacion() + " | " + aux.getEnlaceD() + " ] , ");
			aux = aux.getEnlaceD();
			hayDato = true;
			id++;
		}
		System.out.println("");
		return hayDato;
	}
	
	public void borrar(int val){
		if(inicio == null) System.out.println("Lista vacía.");
		else{
			int ban = buscar(val);
			if(ban == 1){
				if(inicio == fin) {
					inicio = null;
					fin = null;
				}else{
					if(n == inicio){
						inicio = inicio.getEnlaceD();
						inicio.setEnlaceI(null);
					}else{
						if(n == fin){
							fin = fin.getEnlaceI();
							fin.setEnlaceD(null);
						}else{
							aux = n.getEnlaceI();
							aux.setEnlaceD(n.getEnlaceD());
							aux = n.getEnlaceD();
							aux.setEnlaceI(n.getEnlaceI());
							aux = null;
						}
						n = null;
					}
				}
				System.out.println("Eliminado correctamente.");
				nodos--;
				System.out.println("Nodos creados: "+nodos);
			}
		}
	}
	
	public int buscar(int val){
		n = inicio;
		int ban = 0;
		while(n != null && ban == 0){
			if(n.getInformacion() == val) ban = 1;
			else n = n.getEnlaceD();
		}
		if(ban == 0) System.out.println("Valor no encontrado.");
		return ban;
	}
	
}
