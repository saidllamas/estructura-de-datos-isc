package com.validaciones.saidllamas;

import java.util.Scanner;

public class Validaciones {
	
	protected void soloLetras(String value){
	}

	protected void soloLetras(String msj, String value){
		System.out.println(msj);
	}
	
	protected void soloLetras(String value, int limite){
		
	}
	
	protected void soloLetras(String msj, String value, int limite){
		System.out.println(msj);
	}
	
	protected int soloNumeros(){
		/***
		 * Solicita entradas hasta que se ingrese un numero
		 */
		int val = -1;
		do{
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		return val;
	}
	
	protected boolean soloNumeros(String value){
		/***
		 * Retorna true si hubo error, si no hubo error entonces false
		 */
		int val;
		boolean isError = false;
		try {
			val = Integer.parseInt(value);
		} catch (Exception e) {
			System.out.println("Ocurrio un error.");
			isError = true;
		}
		return isError;
	}
	
	protected int soloNumeros(String msj, String value){
		/***
		 * Solicita entradas hasta que se ingrese un numero
		 */
		int val = -1;
		do{
			try {
				Scanner en = new Scanner(System.in);
				val = en.nextInt();
			} catch (Exception e) {
				System.out.println("Ocurrio un error.");
			}
		}while(val == -1);
		return val;
	}

	protected void soloNumeros(String value, int inicio, int limite){
		
	}
	
	protected void soloNumeros(String msj, String value, int inicio, int limite){
		System.out.println(msj + inicio + ", hasta: " + limite);
	}
	
	protected void soloOperadoresMatematicos(String value){
		
	}
	
	protected void soloOperadoresMatematicos(String msj, String value){
		System.out.println(msj);
	}
	
	protected void soloOperadoresMatematicos(String value, int limite){
		
	}
	
	protected void soloOperadoresMatematicos(String msj, String value, int limite){
		System.out.println(msj);
	}
	
	protected void soloOperadoresRelacionales(String value){
		
	}
	
	protected void soloOperadoresRelacionales(String value, int limite){
		
	}
	
	public String getInfo(){
		String msj = "Creado por J. Said Llamas Manriquez. I.S.C. 2016";
		return msj;
	}
	
	public void printInfo(){
		System.out.println("Creado por J. Said Llamas Manriquez. I.S.C. 2016");
	}
}
