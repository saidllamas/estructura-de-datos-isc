package com.saidllamas.recursividad;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		System.out.println("Said Llamas Manriquez");
		System.out.println("El siguiente programa no acepta letras, numeros negativos o 0 ni numeros mayores a 100");
		entrada();
	}
	
	public static void entrada(){
		int n = 0;
		do{
			System.out.println("Ingrese la pontencia a elevar el 2: _____");
			Scanner e = new Scanner(System.in);
			try {
				n = e.nextInt();
			} catch (Exception e2) {
				System.out.println("Error, se detecto una letra");
				entrada();
			}
		}while(validar(n));
		System.out.println("Iterativo: "+iterativo(n));
		System.out.println("Recursivo: "+recursivo(n));
	}
	
	private static long iterativo(int potencia){
		long res = 2;
		for (int i = 1; i < potencia; i++) {
			try {
				res = res * 2;	
			} catch (ArithmeticException e) {
				System.out.println("Ocurrio un error al intentar realizar la operacion");
			}
		}
		return res;
	}
	
	private static long recursivo(int num){
		if(num <= 1) return 2;
		else {
			return 2 * recursivo(num-1);
		}
	}
	
	public static boolean validar(int n){
		if(n<=0||n>100) return true;
		else return false;
	}
	
}
