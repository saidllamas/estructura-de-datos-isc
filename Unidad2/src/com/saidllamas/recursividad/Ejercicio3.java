package com.saidllamas.recursividad;

import java.util.Scanner;

public class Ejercicio3 {
	
	static int min = 0, max = 100;

	public static void main(String[] args) {
		System.out.println("Said Llamas Manriquez");
		System.out.println("N > 0; N < 100");
		System.out.println("M > 1; M < 100");
		System.out.println("El siguiente programa suma X numero N veces");
		inicio();
	}
	
	private static void inicio(){
		int n = entradaN();
		int m = entradaM();
		System.out.println("Iterativo: "+iterativo(n,m));
		System.out.println("Recursivo: "+recursivo(n,m));
	}
	
	private static int entradaN(){
		int numSumar = 0;
		do{
			System.out.println("Ingrese numero a sumar: _____");
			Scanner e = new Scanner(System.in);
			try {
				numSumar = e.nextInt();
			} catch (Exception e2) {
				System.out.println("Error, se detecto una letra");
				entradaN();
			}
		}while(validar(numSumar, 0, 100));
		return numSumar;
	}
	private static int entradaM(){
		int numVeces = 0;
		do{
			System.out.println("Ingrese numero de veces a sumar : _____");
			Scanner e = new Scanner(System.in);
			try {
				numVeces = e.nextInt();
			} catch (Exception e2) {
				System.out.println("Error, se detecto una letra");
				entradaM();
			}
		}while(validar(numVeces, 1, 100));
		return numVeces;
	}
	
	private static boolean validar(int n, int min, int max){
		if(n < min || n > max) return true;
		else return false;
	}

	private static int iterativo(int n, int m){
		int res = 0;
		//int res = n * m;
		for (int i = 1; i <= m; i++) {
			res += n; 
		}
		return res;
	}
	private static int recursivo(int n, int m){
		if(m<=1) return n;
		else return n + recursivo(n,m-1);
	}
}
