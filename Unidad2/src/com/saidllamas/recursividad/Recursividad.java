package com.saidllamas.recursividad;

import java.util.Scanner;

public class Recursividad {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Ingresar numero");
		int n = entrada.nextInt();
		//System.out.println(recursivo(n)); //recursivo
		System.out.println(recursivo(n));
		imprimir(n); //iterativo
	}
	
	public static void imprimir(int n){
		int i;
		for (i =1; i <= n; i++) {
			System.out.print(i+", ");
		}
	}
	
	
	private static int recursivo(int numero) {
		int r = numero;
		System.out.println(r);
		if(numero <= 1) return 0;
		else{
			//int r = numero; 
			//System.out.print(r-- +", ");
			return recursivo(numero-1);
		}
	}

}
