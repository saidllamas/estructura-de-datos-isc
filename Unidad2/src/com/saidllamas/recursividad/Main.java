package com.saidllamas.recursividad;

public class Main {

	public static void main(String[] args) {
		System.out.println(factorial(3));
	}
	
	public static long factorial(int n){
		long res;
		if(n==0) return 1;
		else{
			res = n * factorial(n-1);
			return res;
		}
	}

}
