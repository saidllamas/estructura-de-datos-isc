package com.pruebas;

import java.util.Scanner;

public class Ejem1 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int numero = 0, aux = 1;
		System.out.println("Ingrese un numero");
		do{
			try {
				numero = entrada.nextInt();
			} catch (Exception e) {
				aux = 0;
				System.out.println("Ingrese numero nuevamente");
			}
		}while(aux == 0);
		System.out.println(recursividad(numero));
	}

	private static int recursividad(int numero) {
		if(numero <= 1) return 1;
		else{
			int r = numero; 
			System.out.println(r--);
			return recursividad(r--);
		}
		//return numero;
	}
	
	

}
