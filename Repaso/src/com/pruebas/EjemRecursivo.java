package com.pruebas;

public class EjemRecursivo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Sumatorio(10));
	}
	
	private static int Sumatorio(int iNumero){
		  if (iNumero==0)
		    return 0;
		  else
		    return iNumero + Sumatorio(iNumero-1);
		}

}
