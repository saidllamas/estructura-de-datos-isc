package com.pruebas;

public class Recursividad {

	public static void main(String[] args) {
		System.out.print(factorial(4));
	}
	
	public static double factorial(int n){
	    if (n==0)
	        return 1;
	    else
	        return n*(factorial(n-1));
	}

}
