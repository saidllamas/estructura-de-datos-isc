# README #

Pongo en libertad este repositorio con el fin de que mis compañeros de clase o toda aquella persona que conozca mi usuario en esta plataforma pueda acceder a mis codigos que ire realizando en la materia de Estructura de Datos.

### Informacion ###

* Sientete libre de clonar, copiar y contribuir a este repo
* Mi URL personal [saidllamas.com.mx](Link URL)
* URL de mi equipo [developermidnight.22web.org](Link URL)

### Como usar este repo ###

* Por lo regular tratare de dividir en carpetas los programas que vaya realizando
* Cada carpeta sera de un dia de clase
* Cada paquete correspondera a un tema especifico
* Para poder correr cualquier codigo tendras dos opciones:
1. Copiar el codigo de interes y pegarlo en tu IDE
2. Descargar el repo y direccionar tu IDE a esa carpeta

### Autor ###

* Jesús Said Llamas Manriquez, correo personal saidllamas14@gmail.com correo de trabajo: developSaid14@gmail.com

### Agradecer no cuesta ###
Si te ha servido alguno de mis codigos enviame un correo y saludame, me haria muy feliz saberlo.