package com.saidllamas.ed;

import java.util.Scanner;

public class Principal3 {

	public static void main(String[] args) {
		int matriz[][]= new int[2][5];
		int matriz2[][]= new int[2][5];
		Scanner entrada = new Scanner(System.in);
		
		//llenado de primer matriz
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.println("Ingrese un valor para la casilla: "+(i+1)+","+(j+1));
				matriz[i][j] = entrada.nextInt();
			}
		}
		//llenado de segunda matriz
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.println("Ingrese un valor para la casilla: "+(i+1)+","+(j+1));
				matriz2[i][j] = entrada.nextInt();
			}
		}
		
		//impresion y evaluacion de igualdad
		System.out.println("Primer matriz");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(matriz[i][j]);
				System.out.print("  ");
			}
			System.out.println("");
		}
		System.out.println("Segunda matriz");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(matriz2[i][j]);
				System.out.print("  ");
			}
			System.out.println("");
		}
		//comparacion de matrices
		String msj = "Matrices iguales.";
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 5; j++) {
				if(matriz[i][j]!=matriz2[i][j]){
					msj = "Matrices NO iguales.";
				}
			}
		}
		System.out.println(msj);
		
	}

}
