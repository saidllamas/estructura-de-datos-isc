package com.saidllamas.ed;

import java.util.Scanner;

public class Principal2 {
	static private String[] cadenas = new String[3];

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String cadena1, cadena2, cadena3;
		do{
			System.out.println("Ingrese primer cadena");
			cadena1 = entrada.nextLine();
		}while(cadena1.length() > 5 | cadena1.length() <= 0);
		cadenas[0] = cadena1;
		do{
			System.out.println("Ingrese segunda cadena");
			cadena2 = entrada.nextLine();
		}while(cadena2.length() > 5 | cadena2.length() <= 0);
		cadenas[1] = cadena2;
		do{
			System.out.println("Ingrese tercer cadena");
			cadena3 = entrada.nextLine();
		}while(cadena3.length() > 5 | cadena3.length() <= 0);
		cadenas[2] = cadena3;
		
		ordenar();
		
		for (String cadena : cadenas) {
			System.out.println(cadena);			
		}
	}
	
	 static void ordenar() {
	     for(int j = 0; j < 2; j++) {
	         for(int i = 0; i < 2-j; i++) {
	             if (cadenas[i].compareTo(cadenas[i+1])>0) {
	                 String aux;
	                 aux=cadenas[i];
	                 cadenas[i]=cadenas[i+1];
	                 cadenas[i+1]=aux;
	             }//if
	         }//for--
	     }//for
	 }//ordenar

}
