package com.saidllamas.ed;

import java.util.Scanner;

public class Principal4 {

	public static void main(String[] args) {
		int numero;
		Scanner entrada = new Scanner(System.in);
		do{
			System.out.print("Ingrese un numero comprendido entre 1 y 100");
			numero = entrada.nextInt();
		}while(!validar(numero));
		System.out.println("�Correcto! Fin del programa.");
	}
	
	static boolean validar(int numero){
		if(numero > 100){
			System.out.println("Error, ingrese un numero menor");
			return false;
		}
		else if (numero < 1){
			System.out.println("Error, ingrese un numero mayor");
			return false;
		}
		else return true;
	}

}
