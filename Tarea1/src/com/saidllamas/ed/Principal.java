package com.saidllamas.ed;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String cadena1, cadena2;
		do{
			System.out.println("Ingrese primer cadena");
			cadena1 = entrada.nextLine();
		}while(cadena1.length() > 10 | cadena1.length() <= 0);
		do{
			System.out.println("Ingrese segunda cadena");
			cadena2 = entrada.nextLine();
		}while(cadena2.length() > 10 | cadena2.length() <= 0);
		
		if(cadena1.length() == cadena2.length()) System.out.println("Tienen el mismo tama�o");
		else if(cadena1.length() < cadena2.length()) System.out.println(cadena1+cadena2);
		else  System.out.println(cadena2+cadena1);
	}

}
