package com.saidllamas.ed;

import java.util.Scanner;

public class Principal5 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String c = "";
		do{
			System.out.println("Ingrese una letra");
			c = entrada.nextLine();
		}while(!validar(c));
		System.out.println("Fin del programa.");
	}
	
	static boolean validar(String c){
		String original = c;
		String alterada = c.toUpperCase();
		if(original.equals(alterada)){
			System.out.println("Error, caracter en mayusculas");
			return false;
		}
		else{
			System.out.println("Correcto");
			return true;
		}
	}
}
